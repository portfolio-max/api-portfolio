package main

import (
	// db "./app/common/libs/db.go"
	"context"
	// "encoding/json"
	// "math/rand"
	"log"
	// "strconv"
	"net/http"
	"github.com/gorilla/mux"
	"gitlab.com/api-portfolio/app/controllers"
	"fmt"
	
	"go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    // "go.mongodb.org/mongo-driver/bson"
)

// ...Experience
type Experience struct {
	ID       string `json:"id"`
	Title    string `json:"title"`
	Desc     string `json:"desc"`
	User   	*User 	`json:"user"`
	Position string `json:"position"`
	Division string `json:"division"`
}

// ...User
type User struct {
	Firstname string `json:"firstname"`
	Lastname string	`json:"lastname"`
	Email string `json:"email"`
	Password string `json:"password"`
}

func connect()(*mongo.Database, error) {
	clientOptions := options.Client()
    clientOptions.ApplyURI("mongodb://mex:admin123@ds263048.mlab.com:63048/mex-web?retryWrites=false")
    client, err := mongo.NewClient(clientOptions)
    if err != nil {
        return nil, err
    }

    err = client.Connect(context.TODO())
    if err != nil {
        return nil, err
	}
	
	fmt.Println("Connected to MongoDB!")

    return client.Database("mex-web"), nil
}

func definedRoute (client *mongo.Database) (routes *mux.Router) {
	routes = mux.NewRouter()

	controllers.DbClient = client

	routes.HandleFunc("/v1/profiles", controllers.GetProfiles).Methods("GET")
	routes.HandleFunc("/v1/profile", controllers.GetProfileById).Methods("GET")
	// routes.HandleFunc("/v1/profile/experience", updateOrAddExperience).Methods("POST")
	// routes.HandleFunc("/v1/profile/project", updateProject).Methods("POST")
	// routes.HandleFunc("/v1/profile/education", updateEducation).Methods("POST")
	return
}

func main() {
	client, err := connect() 

	if err != nil {
		log.Fatal("ERROR : ", err)
	}

	router := definedRoute(client)
	log.Fatal(http.ListenAndServe(":8080", router))
}
