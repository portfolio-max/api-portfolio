package profiles

// ...User
type User struct {
	Firstname string `json:"firstname"`
	Lastname string	`json:"lastname"`
	Email string `json:"email"`
	Password string `json:"password"`
}
