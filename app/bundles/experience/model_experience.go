package experience

import User "../profiles/model_profiles"
// ...Experience
type Experience struct {
	ID       string `json:"id"`
	Title    string `json:"title"`
	Desc     string `json:"desc"`
	User   *User `json:"user"`
	Position string `json:"position"`
	Division string `json:"division"`
}

