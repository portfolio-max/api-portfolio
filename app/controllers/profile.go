package controllers

import (
	"context"
	"encoding/json"
	"net/http"
	"fmt"
	// "strconv"
	// "log"

	"go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/bson"
)

var DbClient *mongo.Database

// ...Experience
type Experience struct {
	ID       string `json:"id"`
	Title    string `json:"title"`
	Desc     string `json:"desc"`
	User   	*User 	`json:"user"`
	Position string `json:"position"`
	Division string `json:"division"`
}

// ...User
type User struct {
	Firstname string `json:"firstname"`
	Lastname string	`json:"lastname"`
	Email string `json:"email"`
	Password string `json:"password"`
}

func GetProfiles(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")

	ctx := context.Background()

	resp, err := DbClient.Collection("experience").Find(ctx, bson.M{})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	defer resp.Close(ctx)

	var experience []Experience
	for resp.Next(ctx) {
		var exp Experience
		resp.Decode(&exp)
		experience = append(experience, exp)
	}

	if err := resp.Err(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(experience)
}

func GetProfileById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")
	ctx := context.Background()

	paramsID := r.URL.Query()["id"]; // get params

	fmt.Println("id : ", paramsID[0])
	json.NewEncoder(w).Encode(&Experience{})

	resp, err := DbClient.Collection("users").Find(ctx, bson.M{ "_id": paramsID[0] })

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	defer resp.Close(ctx)

	var users []User
	for resp.Next(ctx) {
		var usr User
		resp.Decode(&usr)
		users = append(users, usr)
	}

	if err := resp.Err(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(users)
}

func updateOrAddExperience(w http.ResponseWriter, r *http.Request) {
	
}

func updateProject(w http.ResponseWriter, r *http.Request) {

}

func updateEducation(w http.ResponseWriter, r *http.Request) {

}