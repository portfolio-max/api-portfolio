package dbmysql

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"fmt"
)

func connection()(db *sql.DB){
	fmt.Println("Connection MYSQL : Starting")
	db, err := sql.Open("mysql", "username:password@tcp(127.0.0.1:3306)/test")
    // if there is an error opening the connection, handle it
	// should be able to check if connection is success return db and if connection failed should return Error message
	
	if err != nil {
		fmt.Println("Start Connection MYSQL : Failed")
        panic(err.Error())
    }
	
	// defer the close till after the main function has finished
    // executing
	defer db.Close()
	
	return db;
}